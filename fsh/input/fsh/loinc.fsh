Alias: $sutermserv_project = https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-project
Alias: $sutermserv_dataset = https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-dataset
Alias: $sutermserv_license = https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-license

RuleSet: LOINC(version)
* ^url = "http://loinc.org"
* ^version = "{version}"
* ^status = #active
* ^copyright = "This content from LOINC® is copyright © 1995 Regenstrief Institute, Inc. and the LOINC Committee, and available at no cost under the license at http://loinc.org/terms-of-use"
* ^caseSensitive = true
* ^valueSet = "http://loinc.org/vs"
* ^content = #not-present
* ^hierarchyMeaning = #is-a
* ^compositional = false
* ^versionNeeded = false
* ^publisher = "Regenstrief Institute, Inc."
* ^meta.tag[+].system = $sutermserv_project
* ^meta.tag[=].code = #international
* ^meta.tag[=].display = "International standard terminology"
* ^meta.tag[+].system = $sutermserv_dataset
* ^meta.tag[=].code = #loinc
* ^meta.tag[=].display = "Versions of LOINC"
* ^meta.tag[+].system = $sutermserv_license
* ^meta.tag[=].code = #https://mii-termserv.de/licenses#loinc
* ^meta.tag[=].display = "LOINC license"
* ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/cqf-scope"
* ^extension[=].valueString = "@mii-termserv/loinc"

CodeSystem: LOINC_2_77
Title: "LOINC"
Description: "LOINC is a common language (set of identifiers, names, and codes) for clinical and laboratory observations."
Id: LOINC-2.77
* insert LOINC(2.77)

CodeSystem: LOINC_2_78
Title: "LOINC"
Description: "LOINC is a common language (set of identifiers, names, and codes) for clinical and laboratory observations."
Id: LOINC-2.78
* insert LOINC(2.78)
